$(document).ready(function(){
	$('#open-menu').on('click',function(e) {
		e.preventDefault();

		var $menu = $(this).parent().find('.hidden-menu'),
			$icon = $(this).find('.fa');

		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			$icon.removeClass('fa-minus');
			$menu.slideUp('slow');
		}else {
			$(this).addClass('active');
			$icon.addClass('fa-minus');
			$menu.slideDown('slow');
		}
		
	});



	$(window).on('scroll',function() {

		// var top = $(this).scrollTop();
		// if(top > 55) {
		// 	$('#main-nav-mobile').addClass('open');
		// }else {
		// 	$('#main-nav-mobile').removeClass('open');
		// 	$('#main-nav').removeClass('open');
		// 	$('#main-nav-mobile').removeClass('active');
		// 	$('#fixed-nav-top').removeClass('hidden')
		// }
	});

	$('#open-nav').on('click',function(e) {
		e.preventDefault();
		$('#main-nav').addClass('open');
		$('#main-nav-mobile').addClass('active');
		$('#fixed-nav-top').addClass('hidden');
	});

	$('#close-nav').on('click',function(e) {
		e.preventDefault();
		$('#main-nav').removeClass('open');
		$('#main-nav-mobile').removeClass('active');
		$('#fixed-nav-top').removeClass('hidden');
	});


	/**
	*	checkbox & radio - style
	**/

	 $('.icheckbox').iCheck({
	    checkboxClass: 'icheckbox_minimal',
	    radioClass: 'icheckbox_minimal',
	    increaseArea: '20%' // optional
	  });

	 $('.iradio').iCheck({
	    checkboxClass: 'iradio_minimal',
	    radioClass: 'iradio_minimal',
	    increaseArea: '20%' // optional
	  });

	 $('.icheckbox.red').iCheck({
	    checkboxClass: 'icheckbox_minimal red',
	    radioClass: 'icheckbox_minimal red',
	    increaseArea: '20%' // optional
	  });
	 
	 $('.iradio.red').iCheck({
	    checkboxClass: 'iradio_minimal red',
	    radioClass: 'iradio_minimal red',
	    increaseArea: '20%' // optional
	  });


	 /**
	 * faq- page
	 **/
	 $('.faq-question').on('click',function(e) {
	 	e.preventDefault();
	 	var $faq = $(this).next('.faq-reponse');

	 	if($(this).hasClass('open')) {
	 		$faq.slideUp();
	 		$(this).removeClass('open');
	 	}else {
	 		$(this).addClass('open');
	 		$faq.slideDown();
	 	}
	 });


	 /**
	 * input -file
	 **/
	$(".row-form [type=file]").on("change", function(){
		var file = this.files[0].name;
		var dflt = $(this).attr("placeholder");
			if($(this).val()!=""){
			$(this).next().text(file);
		} else {
			$(this).next().text(dflt);
		}
	});


	/**
	* open-summary
	**/

	$('#open-summary').on('click',function(e) {
		e.preventDefault();

		if($('.right-article').hasClass('open')) {
			$('.right-article').removeClass('open');
		} else {
			$('.right-article').addClass('open')
		}
	});

	$('#close-summary').on('click',function() {
		$('.right-article').removeClass('open');
	});
	 


	 $('.ak-add-address').on('click',function(e) {
	 	e.preventDefault();
	 	var $element = $(this).parent().next();
	 	if(!$element.hasClass('active')) {
	 		$element.slideDown();
	 		$element.addClass('active');
	 	}else {
	 		$element.slideUp();
	 		$element.removeClass('active');
	 	}
	 });


	 /**
	 * ouvrir sommaire
	 **/

	 $('#btn-show-summary').on('click',function(e) {
	 	e.preventDefault();
	 	$('#ak-wrap-sommaire').slideDown('slow');
	 });

	 $('#ak-close-summary').on('click',function() {
	 	$('#ak-wrap-sommaire').slideUp('slow');
	 });



	 $('#lost_mail').on('click',function(){
	 	if ($(this).is(':checked')) {
	 		$('.lost_mail').show();
	 	}else {
	 		$('.lost_mail').hide();
	 	}
	 	
	 });


	 $('.ak-open-contact').on('click',function(e){
		e.preventDefault();
		$('#wrap-popup-contact').fadeIn();
		$('#popup-contact').addClass('open');
	});


	$('#close_contact, #wrap-popup-contact').on('click',function(e) {
		e.preventDefault();
		$('#wrap-popup-contact').fadeOut();
		$('#popup-contact').removeClass('open');
	});

	$('#newsletter_form').on('submit',function(e){
	    e.preventDefault();
	    var $this = $(this),
	    	$email = $this.find('.email');
    	$this.find('button').addClass('loading');
	    var datarequest = $.ajax({
	        url: $(this).attr('action'), 
	        type: $(this).attr('method'), 
	        data: $(this).serialize(), 
	        success: function(html) { 
	            $email.val(html);           
	            $this.find('button').removeClass('loading');
	            setTimeout(function(){
	                $email.val('')
	            },1900);
	        }
	    });
	});


    $('.account-info').on('click',function(e) {
      e.preventDefault();
      if($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).next('.form-login').slideUp();
      }else {
        $(this).addClass('active');
        $(this).next('.form-login').slideDown();
      }
    });


});

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.last-post article');
});


$(window).resize(function(){
  equalheight('.last-post article');
});