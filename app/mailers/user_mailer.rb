class UserMailer < ActionMailer::Base
  default from: "contact.lenouvelespritpublic@gmail.com"
 
  def subscribe_confirmation(email)
    mail(to: email, subject: "Merci de nous soutenir!")
  end

  def subscribe(email)
    @email = email
    mail to: "contact.lenouvelespritpublic@gmail.com", subject: "Un nouveau soutien"
  end

  def booking_confirmation(email)
    mail(to: email, subject: "Votre soutien a bien été enregistré !")
  end

  def booking(email)
    @email = email
    mail to: "contact.lenouvelespritpublic@gmail.com", subject: "Un nouveau soutien"
  end


  # private
  #   def apply_params
  #     params.require(:user_mailer).permit(:email)
  #   end

end
