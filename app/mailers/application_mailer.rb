class ApplicationMailer < ActionMailer::Base
  default from: 'contact.lenouvelespritpublic@gmail.com'
  layout 'mailer'
end
