class Intervenant < ApplicationRecord
	has_many :books
	has_many :podcasts
	has_many :abonnes
	has_many :rubriques
end
