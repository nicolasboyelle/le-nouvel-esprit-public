class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  before_action :set_link

  def index
    @books = Book.all.includes(:podcast).order('podcasts.created_at desc').page(params[:page]).per(12)
  end

  def show
    
  end

  def new
    @book = book.new
  end

  def edit
  end

  def create
    @book = book.new(book_params)
    
    if @book.save
      redirect_to @book, notice: 'book was successfully created.'
    else
      render :new
    end
  end

  def update
    if @book.update(book_params)
      redirect_to @book, notice: 'book was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @book.destroy
    redirect_to books_url, notice: 'book was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:titre, :intervenant_id, :podcast_id, :description, :image)
    end

    def set_link
      @public_link = Public.where(live: true).order(created_at: :desc).first
    end 
end
