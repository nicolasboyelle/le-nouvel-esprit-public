class PodcastsController < ApplicationController
  before_action :set_podcast, only: [:show, :edit, :update, :destroy]
  before_action :set_link

  def index
    if params[:search]
      @podcasts = Podcast.all.where('title LIKE ?', "%#{params[:search]}%").order(created_at: :desc).page(params[:page]).per(10)
    else
      @podcasts = Podcast.all.order(created_at: :desc).page(params[:page]).per(10)
    end
  end

  def show
  end

  def new
    @podcast = Podcast.new
  end

  def thematiques

    if params[:search]
      @podcasts = Podcast.all.where("title like ?", "%Thématique%").where('title LIKE ?', "%#{params[:search]}%").order(created_at: :desc).page(params[:page]).per(10)
    else
      @podcasts = Podcast.all.where("title like ?", "%Thématique%").order(created_at: :desc).page(params[:page]).per(10)
    end
  end

  def edit
  end

  def create
    @podcast = Podcast.new(podcast_params)

    if @podcast.save
      redirect_to @podcast, notice: 'Podcast was successfully created.'
    else
      render :new
    end
  end

  def update
    if @podcast.update(podcast_params)
      redirect_to @podcast, notice: 'Podcast was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @podcast.destroy
    redirect_to podcasts_url, notice: 'Podcast was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_podcast
      @podcast = Podcast.find(params[:id])
    end

    def set_link
      @public_link = Public.where(live: true).order(created_at: :desc).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def podcast_params
      params.require(:podcast).permit(:title, :lien, :title1, :intro1, :title2, :intro2, :updated_at, :kontildondit1, :kontildondit2, :document, :pdf, :thematique)
    end
end
