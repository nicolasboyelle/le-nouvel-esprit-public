class AbonnesController < ApplicationController
  before_action :set_abonne, only: [:show, :edit, :update, :destroy]
  before_action :set_link

  def index
    @abonnes = Abonne.all.order(created_at: :desc).page(params[:page]).per(5)
  end

  def show
  end

  def new
    @abonne = Abonne.new
  end

  def edit
  end

  def create
    @abonne = Abonne.new(abonne_params)
    
    if @abonne.save
      redirect_to @abonne, notice: 'Podcast was successfully created.'
    else
      render :new
    end
  end

  def update
    if @abonne.update(abonne_params)
      redirect_to @abonne, notice: 'Podcast was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @abonne.destroy
    redirect_to pabonnes_url, notice: 'Podcast was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_abonne
      @abonne = Abonne.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def abonne_params
      params.require(:abonne).permit(:title, :lien, :title1, :intro1, :title2, :intro2)
    end

    def set_link
      @public_link = Public.where(live: true).order(created_at: :desc).first
    end

end
