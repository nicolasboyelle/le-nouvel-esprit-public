class PagesController < ApplicationController
	before_action :set_link, :set_mailchimp


	def home
		@podcast = Podcast.all.order(created_at: :desc).first
	end
	
	def partners
	end

	def abonnement
	end

	def updatecard
	end

	def mentions
	end

	def soutiens
	end

	def lettre
	end

	def lettretest
	end

	def subscribed
    UserMailer.subscribe_confirmation(params[:email]).deliver
    UserMailer.subscribe(params[:email]).deliver
    redirect_to :action => "home"
	end

	def letsencrypt
	  render text: params[:id]+'.ylwsIZ1qM8410SY4fhGjVuvDfZoYpC6DvIEe4BSXSek'
	end

	private
    def set_link
      @public_link = Public.where(live: true).order(created_at: :desc).first
    end

    def set_mailchimp
      @mailchimp_link = Mailchimp.order(created_at: :desc).first
    end 

end