class BadasController < ApplicationController
  before_action :set_bada, only: [:show, :edit, :update, :destroy]
  before_action :set_link

  def index
    @badas = Bada.all.includes(:podcast).order('podcasts.created_at desc').page(params[:page]).per(5)
  end

  def show
  end

  def new
    @bada = Bada.new
  end

  def edit
  end

  def create
    @bada = Bada.new(bada_params)
    
    if @bada.save
      redirect_to @bada, notice: 'Bada was successfully created.'
    else
      render :new
    end
  end

  def update
    if @bada.update(bada_params)
      redirect_to @bada, notice: 'Bada was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @bada.destroy
    redirect_to badas_url, notice: 'Bada was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bada
      @bada = Bada.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bada_params
      params.require(:bada).permit(:title, :lien, :podcast_id, :transcript)
    end

    def set_link
      @public_link = Public.where(live: true).order(created_at: :desc).first
    end 
end
