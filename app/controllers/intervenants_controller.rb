class IntervenantsController < ApplicationController
	before_action :set_link

	def show
		@intervenant = Intervenant.find(params[:id])
		@intervenants = Intervenant.all.order(:nom)
	end

	private
    def set_link
      @public_link = Public.where(live: true).order(created_at: :desc).first
    end	
end