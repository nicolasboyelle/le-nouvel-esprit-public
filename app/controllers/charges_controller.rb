class ChargesController < ApplicationController
  def new
  end

  def create
    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :address => params[:order_address],
      :card  => params[:stripeToken],
      :plan => params[:stripePlan]
    )

    # charge = Stripe::Charge.create(
    #   customer:     customer.id,
    #   amount:       params[:stripeAmount],
    #   description:  'Le Nouvel Esprit public',
    #   currency:     'eur'
    # )

    UserMailer.booking_confirmation(params[:stripeEmail]).deliver
    UserMailer.booking(params[:stripeEmail]).deliver

    redirect_to thanks_path

    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to charges_path
  end

  def thanks
  end

  def plans
    @stripe_list = Stripe::Plan.all
    @plans = @stripe_list[:data]
  end
  
end
