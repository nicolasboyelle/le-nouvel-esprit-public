class AddTranscriptToBadas < ActiveRecord::Migration[5.0]
  def change
    add_column :badas, :transcript, :text
  end
end
