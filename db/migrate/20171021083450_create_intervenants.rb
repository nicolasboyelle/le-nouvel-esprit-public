class CreateIntervenants < ActiveRecord::Migration[5.0]
  def change
    create_table :intervenants do |t|
      t.string :nom
      t.string :description
      t.string :lien

      t.timestamps
    end
  end
end
