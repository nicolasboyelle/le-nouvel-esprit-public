class AddPodcastIdToBreves < ActiveRecord::Migration[5.0]
  def change
    add_column :breves, :podcast_id, :integer
    add_index :breves, :podcast_id
  end
end
