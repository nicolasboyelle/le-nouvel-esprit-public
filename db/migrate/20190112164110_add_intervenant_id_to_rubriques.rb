class AddIntervenantIdToRubriques < ActiveRecord::Migration[5.0]
  def change
    add_column :rubriques, :intervenant_id, :integer
  end
end
