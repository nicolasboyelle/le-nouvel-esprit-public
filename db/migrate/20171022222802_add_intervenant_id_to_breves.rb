class AddIntervenantIdToBreves < ActiveRecord::Migration[5.0]
  def change
    add_column :breves, :intervenant_id, :integer
    add_index :breves, :intervenant_id
  end
end
