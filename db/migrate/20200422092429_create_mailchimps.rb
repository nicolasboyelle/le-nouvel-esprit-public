class CreateMailchimps < ActiveRecord::Migration[5.0]
  def change
    create_table :mailchimps do |t|
      t.string :lien

      t.timestamps
    end
  end
end
