class AddResumeToRubriques < ActiveRecord::Migration[5.0]
  def change
    add_column :rubriques, :resume, :string
  end
end
