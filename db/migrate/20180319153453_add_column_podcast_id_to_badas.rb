class AddColumnPodcastIdToBadas < ActiveRecord::Migration[5.0]
  def change
    add_column :badas, :podcast_id, :integer
  end
end
