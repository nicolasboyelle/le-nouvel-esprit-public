class RenameBrevesToBooks < ActiveRecord::Migration[7.0]
  def change
    rename_table :breves, :books
  end
end