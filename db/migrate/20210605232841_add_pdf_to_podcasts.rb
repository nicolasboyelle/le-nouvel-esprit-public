class AddPdfToPodcasts < ActiveRecord::Migration[5.0]
  def change
    add_column :podcasts, :pdf, :string
  end
end
