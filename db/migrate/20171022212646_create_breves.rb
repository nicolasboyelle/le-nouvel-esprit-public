class CreateBreves < ActiveRecord::Migration[5.0]
  def change
    create_table :breves do |t|
      t.string :titre
      t.string :description

      t.timestamps
    end
  end
end
