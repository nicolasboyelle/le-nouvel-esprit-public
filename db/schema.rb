# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_12_23_153559) do
  create_table "abonnes", force: :cascade do |t|
    t.string "title"
    t.string "lien"
    t.string "title1"
    t.string "intro1"
    t.string "title2"
    t.string "intro2"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "badas", force: :cascade do |t|
    t.string "title"
    t.string "lien"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "podcast_id"
    t.text "transcript"
  end

  create_table "books", force: :cascade do |t|
    t.string "titre"
    t.string "description"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "podcast_id"
    t.integer "intervenant_id"
    t.string "lien"
    t.string "image_file_name"
    t.string "image_content_type"
    t.bigint "image_file_size"
    t.datetime "image_updated_at"
    t.index ["intervenant_id"], name: "index_books_on_intervenant_id"
    t.index ["podcast_id"], name: "index_books_on_podcast_id"
  end

  create_table "intervenants", force: :cascade do |t|
    t.string "nom"
    t.string "description"
    t.string "lien"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "mailchimps", force: :cascade do |t|
    t.string "lien"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "podcasts", force: :cascade do |t|
    t.string "title"
    t.string "lien"
    t.string "title1"
    t.string "intro1"
    t.string "title2"
    t.string "intro2"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "kontildondit1"
    t.text "kontildondit2"
    t.string "pdf"
  end

  create_table "publics", force: :cascade do |t|
    t.boolean "live"
    t.string "lien"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "rubriques", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "intervenant_id"
    t.string "resume"
  end

end
