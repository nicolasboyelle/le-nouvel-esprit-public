require 'test_helper'

class KontildonditsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @kontildondit = kontildondits(:one)
  end

  test "should get index" do
    get kontildondits_url
    assert_response :success
  end

  test "should get new" do
    get new_kontildondit_url
    assert_response :success
  end

  test "should create kontildondit" do
    assert_difference('Kontildondit.count') do
      post kontildondits_url, params: { kontildondit: { description1: @kontildondit.description1, description2: @kontildondit.description2 } }
    end

    assert_redirected_to kontildondit_url(Kontildondit.last)
  end

  test "should show kontildondit" do
    get kontildondit_url(@kontildondit)
    assert_response :success
  end

  test "should get edit" do
    get edit_kontildondit_url(@kontildondit)
    assert_response :success
  end

  test "should update kontildondit" do
    patch kontildondit_url(@kontildondit), params: { kontildondit: { description1: @kontildondit.description1, description2: @kontildondit.description2 } }
    assert_redirected_to kontildondit_url(@kontildondit)
  end

  test "should destroy kontildondit" do
    assert_difference('Kontildondit.count', -1) do
      delete kontildondit_url(@kontildondit)
    end

    assert_redirected_to kontildondits_url
  end
end
