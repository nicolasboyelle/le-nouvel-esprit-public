require 'test_helper'

class KontilditsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @kontildit = kontildits(:one)
  end

  test "should get index" do
    get kontildits_url
    assert_response :success
  end

  test "should get new" do
    get new_kontildit_url
    assert_response :success
  end

  test "should create kontildit" do
    assert_difference('Kontildit.count') do
      post kontildits_url, params: { kontildit: { description1: @kontildit.description1, description2: @kontildit.description2 } }
    end

    assert_redirected_to kontildit_url(Kontildit.last)
  end

  test "should show kontildit" do
    get kontildit_url(@kontildit)
    assert_response :success
  end

  test "should get edit" do
    get edit_kontildit_url(@kontildit)
    assert_response :success
  end

  test "should update kontildit" do
    patch kontildit_url(@kontildit), params: { kontildit: { description1: @kontildit.description1, description2: @kontildit.description2 } }
    assert_redirected_to kontildit_url(@kontildit)
  end

  test "should destroy kontildit" do
    assert_difference('Kontildit.count', -1) do
      delete kontildit_url(@kontildit)
    end

    assert_redirected_to kontildits_url
  end
end
