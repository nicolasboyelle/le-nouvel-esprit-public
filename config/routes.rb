Rails.application.routes.draw do
  resources :kontildondits
  mount ForestLiana::Engine => '/forest'
  resources :podcasts
  resources :books
  resources :badas
  resources :abonnes
  resources :rubriques,  :path => "chroniques"
  get '/kitafetoi', to: 'kitafetois#index', as: :kitafetois
  resources :kitafetois
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'pages#home'
  get 'thematiques' => 'podcasts#thematiques', as: :thematiques
  get 'partners' => 'pages#partners', as: :partners
  get 'abonnement' => 'pages#abonnement', as: :abonnement
  get 'updatecard' => 'pages#updatecard', as: :updatecard
  get 'mentions' => 'pages#mentions', as: :mentions
  get '/intervenant/:id', to: 'intervenants#show', as: 'intervenant'
  resources :charges
  resources :updatecards
  get 'thanks', to: 'charges#thanks', as: 'thanks'
  get 'soutiens', to: 'pages#soutiens', as: 'soutiens'
  get '/.well-known/acme-challenge/:id' => 'pages#letsencrypt'
  get 'lettre' => 'pages#lettre', as: :lettre
  get 'lettretest' => 'pages#lettretest', as: :lettretest
end
